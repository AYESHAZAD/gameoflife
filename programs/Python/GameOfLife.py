import numpy as np;
import random as random;
import time

n=1000;
steps=100;
A=np.zeros(n*n);
B=np.zeros(n*n);
#print(A)

for i in range(n*n):
  A[i] = random.randint(0, 1)

start=time.process_time()
#start=time.clock()
for t in range((steps%2)==0):
        for j in range(n):
                jplus=(j+1)%n
                jminus=(j+n-1)%n
                for i in range(n):
                        iplus=(i+1)%n
                        iminus=(i+n-1)%n
                        count=A[j*n+iplus]+A[j*n+iminus]+A[jplus*n+iplus]+A[jplus*n+i]+A[jplus*
                               n+iminus]+A[jminus*n+iplus]+A[jminus*n+i]+A[jminus*n+iminus]
                        if (A[j*n+i]==1 and ( count==2 or count ==3)):
                                B[j*n+i]=1
                        elif (A[j*n+i]==0 and count==3):
                                B[j*n+i]=1
                        else:
                                B[j*n+i]=0
        for j in range(n):
                jplus=(j+1)%n
                jminus=(j+n-1)%n
                for i in range(n):
                   iplus = (i + 1) % n
                   iminus = (i + n - 1) % n
                   count = B[j * n + iplus] + B[j * n + iminus] + B[jplus * n + iminus] + B[jplus * n + i] + B[jplus*n + iplus] \
                             + B[jminus * n + iminus] + B[jminus * n + i] + B[jminus * n + iplus]
                   if (B[j * n + i] == 1 and (count == 2 or count == 3)):
                        A[j * n + i] = 1
                   elif (B[j * n + i] == 0 and count == 3):
                        A[j * n + i] = 1
                   else:
                        A[j * n + i] = 0








stop=time.process_time()
#stop=time.clock()
t_diff=stop-start
print()
#print("The program took ",start)
print("The program took ",(stop-start),"seconds")

#print(A)

